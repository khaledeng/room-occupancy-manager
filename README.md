# Room Occupancy Manager
Room occupancy optimization management tool for dynamically book a premium and economy 
rooms at Hotel.

# Task Requirements
Customers have a certain number of free rooms each night, as well as potential guests 
that would like to book a room for that night.

Hotel clients have two different categories of rooms: **Premium** and **Economy**. Our
hotels want their customers to be satisfied: they will not book a customer willing to pay
**EUR 100** or more for the night into an Economy room. But they will book lower paying
customers into Premium rooms if these rooms would be empty and all Economy rooms will
be filled by low paying customers. Highest paying customers below **EUR 100** will get
preference for the “upgrade”. Customers always 	only have one specific price they are
willing to pay for the night.

# Deliverable features
* Restful API with Spring boot + Java 11. 
* TDD, Mockito, SpringTest.
* Tracking progress through Gitlab commits.
* Swagger API Documentation.
* Request Validation. 
* Exception handling.
* GitLab Deployment Pipeline (build, test, deploy).
* Deployed Service to Heroku Cloud with Gitlab CI (Wanted to demo the task at my heroku account but automatic build not worked with Java11 however I tested it alot with Java8 and was working fine.) Build,test pipline success but deploy stage failed.

# Run Solution
  Application uses maven build for collect dependencies, You can clone the service from Git and build with Maven tool then start the spring boot application. it should start with port 8090.

# Testing Solution 
 1- Solution can be tested very easily by Open Swagger API Documentation from below Link: 
  http://localhost:8090/swagger-ui.html
 
 Open the endpoint and press "Try it now" button, edit the request and press send button to see responses at api response section. 
 Swagger contains how request and resonse should look like.
  
 2- Solution can be tested also through postman collection json located under collection folder in the root path of the application.
    you can import it to postman and test endpoint with the given example. 

