package com.smarthost.roomservice.occupancymanagement.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomOccupancyException extends RuntimeException {

  private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;
  private final HttpStatus httpStatus;
  private final String errorMessage;

  public RoomOccupancyException(String errorMessage) {
    this(errorMessage, HTTP_STATUS);
  }

  public RoomOccupancyException(String errorMessage, HttpStatus httpStatus) {
    super(errorMessage);
    this.httpStatus = httpStatus;
    this.errorMessage = errorMessage;
  }
}
