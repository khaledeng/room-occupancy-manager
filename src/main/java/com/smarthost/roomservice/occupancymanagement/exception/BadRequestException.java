package com.smarthost.roomservice.occupancymanagement.exception;

import org.springframework.http.HttpStatus;

public class BadRequestException extends RoomOccupancyException {

  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public BadRequestException(String errorMessage) {
    super(errorMessage, HTTP_STATUS);
  }
}
