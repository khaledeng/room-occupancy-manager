package com.smarthost.roomservice.occupancymanagement.exception;

import org.springframework.http.HttpStatus;

public class InternalServerErrorException extends RoomOccupancyException {

  private static final HttpStatus HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

  public InternalServerErrorException(String errorMessage) {
    super(errorMessage, HTTP_STATUS);
  }
}
