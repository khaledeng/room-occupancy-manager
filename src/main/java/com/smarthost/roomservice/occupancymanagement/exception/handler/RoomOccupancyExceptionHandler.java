package com.smarthost.roomservice.occupancymanagement.exception.handler;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.smarthost.roomservice.occupancymanagement.exception.model.ErrorResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice
public class RoomOccupancyExceptionHandler extends ResponseEntityExceptionHandler {

  protected ResponseEntity<Object> handleMethodArgumentNotValid(
      MethodArgumentNotValidException ex,
      HttpHeaders headers,
      HttpStatus status,
      WebRequest request) {
    log.error("Caught validation exception", ex);
    List<FieldError> errors = ex.getBindingResult().getFieldErrors();

    ErrorResponse resp = new ErrorResponse();
    resp.setErrorMessage("Validation Error");
    resp.setDetails(buildFriendlyMessage(errors));
    return new ResponseEntity<>(resp, headers, status);
  }

  private static String buildFriendlyMessage(List<FieldError> errors) {
    String validationFields =
        errors
            .stream()
            .map(
                field ->
                    String.format( "%s.%s (%s)",
                        field.getObjectName(), field.getField(), field.getDefaultMessage()))
            .collect(Collectors.joining(", "));
    return String.format("Validation error at fields : %s", validationFields);
  }
}
