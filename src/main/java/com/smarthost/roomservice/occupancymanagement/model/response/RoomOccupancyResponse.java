package com.smarthost.roomservice.occupancymanagement.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(description = "Room Occupancy Response")
public class RoomOccupancyResponse {

  @JsonProperty("usage-premium")
  @ApiModelProperty(
      notes = "Usage Premium  result",
      example = "3 (Eur 200)",
      required = true,
      position = 0)
  private String premiumUsage;

  @JsonProperty("usage-economy")
  @ApiModelProperty(
      notes = "Usage Economy result",
      example = "5 (Eur 320)",
      required = true,
      position = 1)
  private String economyUsage;
}
