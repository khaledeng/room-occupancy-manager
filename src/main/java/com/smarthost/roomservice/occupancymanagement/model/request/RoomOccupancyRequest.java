package com.smarthost.roomservice.occupancymanagement.model.request;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@Builder
@Valid
@ToString
@AllArgsConstructor
@ApiModel(description = "Room Occupancy Request")
public class RoomOccupancyRequest {

  @NotNull
  @Min(value = 0, message = "Premium rooms can't be a negative number")
  @JsonProperty("free-premium-rooms")
  @ApiModelProperty(
      notes = "Free premium rooms available",
      example = "3",
      required = true,
      position = 0)
  private Integer freePremiumRooms;

  @NotNull
  @Min(value = 0, message = "EConomy rooms can't be a negative number")
  @JsonProperty("free-economy-rooms")
  @ApiModelProperty(
      notes = "Free Economy rooms available",
      example = "3",
      required = true,
      position = 1)
  private Integer freeEconomyRooms;
}
