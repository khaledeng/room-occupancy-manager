package com.smarthost.roomservice.occupancymanagement.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  @Value("${com.smarthost.docs.title:Room Occupancy Manager}")
  private String serviceTitle;

  @Value("${com.smarthost.docs.description:API Documentation for Room Occupancy Manager}")
  private String serviceDescription;

  @Value("${com.smarthost.docs.base-package:com.smarthost}")
  private String serviceBasePackage;

  @Value("${com.smarthost.docs.version:1.0.0}")
  private String serviceVersion;

  @Value("${com.smarthost.docs.contact:khaled Mosaad <engkhaledmos3ad@gmail.com>}")
  private String serviceContact;

  public static final String DEFAULT_INCLUDE_PATTERN = "/*/.*";

  /**
   * ApiInfo.
   *
   * @return
   */
  private ApiInfo apiInfo() {
    return new ApiInfoBuilder()
        .title(serviceTitle)
        .version(serviceVersion)
        .description(serviceDescription)
        .contact(new Contact(serviceTitle, null, serviceContact))
        .build();
  }

  /**
   * configureDocket.
   *
   * @return
   */
  @Bean
  public Docket configureDocket() {
    return new Docket(DocumentationType.SWAGGER_2)
        .select()
        .apis(RequestHandlerSelectors.basePackage(serviceBasePackage))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo());
  }
}
