package com.smarthost.roomservice.occupancymanagement.controller;

import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;

@RequestMapping(
    value = {"/room-management"},
    produces = MediaType.APPLICATION_JSON_VALUE)
public interface RoomOccupancyMGMController {

  @ResponseStatus(code = HttpStatus.OK)
  @PostMapping(
      value = "/occupancy",
      consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE)
  RoomOccupancyResponse roomOccupancy(@Valid @RequestBody RoomOccupancyRequest occupancyRequest);
}
