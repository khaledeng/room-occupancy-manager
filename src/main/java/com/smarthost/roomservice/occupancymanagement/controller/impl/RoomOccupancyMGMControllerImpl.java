package com.smarthost.roomservice.occupancymanagement.controller.impl;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.smarthost.roomservice.occupancymanagement.controller.RoomOccupancyMGMController;
import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;
import com.smarthost.roomservice.occupancymanagement.service.RoomOccupancyMGMService;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@Api(
    tags = "Room Occupancy Management API",
    description = "Room Occupancy API for occupy rooms according to customer budgets")
public class RoomOccupancyMGMControllerImpl implements RoomOccupancyMGMController {

  @Autowired private RoomOccupancyMGMService occupancyMGMService;

  @Override
  public RoomOccupancyResponse roomOccupancy(@Valid RoomOccupancyRequest occupancyRequest) {
    log.debug("Receiving request for room occupancy {} ", occupancyRequest.toString());
    return occupancyMGMService.occupyRooms(occupancyRequest);
  }
}
