package com.smarthost.roomservice.occupancymanagement.repository.mock;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Component;

import com.smarthost.roomservice.occupancymanagement.repository.GuestRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class GuestRepositoryMock implements GuestRepository {

  @Override
  public List<Integer> getGuestsPayBudget() {
    log.debug("Mocking Guest payment williness");
    return Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
  }
}
