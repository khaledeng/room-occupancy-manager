package com.smarthost.roomservice.occupancymanagement.repository;

import java.util.List;

public interface GuestRepository {

  List<Integer> getGuestsPayBudget();
}
