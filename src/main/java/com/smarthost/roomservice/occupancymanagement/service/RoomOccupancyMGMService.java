package com.smarthost.roomservice.occupancymanagement.service;

import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;

public interface RoomOccupancyMGMService {

  RoomOccupancyResponse occupyRooms(RoomOccupancyRequest occupancyRequest);
}
