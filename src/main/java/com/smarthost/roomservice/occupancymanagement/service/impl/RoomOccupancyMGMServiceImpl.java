package com.smarthost.roomservice.occupancymanagement.service.impl;

import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;
import com.smarthost.roomservice.occupancymanagement.repository.GuestRepository;
import com.smarthost.roomservice.occupancymanagement.service.RoomOccupancyMGMService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RoomOccupancyMGMServiceImpl implements RoomOccupancyMGMService {

  @Autowired private GuestRepository guestRepository;
  private static final String USAGE_REPRESENTATION_TEMPLATE = "%d (EUR %d)";
  private static final int PREMIUM_MIN_BUDGET_LIMIT = 100;

  @Override
  public RoomOccupancyResponse occupyRooms(RoomOccupancyRequest occupancyRequest) {
    log.debug("Occupying hotel rooms.");
    List<Integer> budgetsSuitPremiumRooms =
        getBudgetsSuitFreeRooms(
            occupancyRequest.getFreePremiumRooms(), budget -> budget >= PREMIUM_MIN_BUDGET_LIMIT);

    List<Integer> budgetsSuitEconomyRooms =
        getBudgetsSuitFreeRooms(
            occupancyRequest.getFreeEconomyRooms(), budget -> budget < PREMIUM_MIN_BUDGET_LIMIT);

    int remainingPremiumRooms =
        occupancyRequest.getFreePremiumRooms() - budgetsSuitPremiumRooms.size();

    if ((remainingPremiumRooms > 0)
        && (occupancyRequest.getFreeEconomyRooms() == budgetsSuitEconomyRooms.size())) {
      log.debug(
          "Detected {} free premium rooms after room occupancy operation", remainingPremiumRooms);
      log.debug("start upgrading process ..");
      budgetsSuitPremiumRooms.addAll(
          budgetsSuitEconomyRooms
              .stream()
              .limit(remainingPremiumRooms)
              .collect(Collectors.toList()));

      int updatedMinBudgetLimit = budgetsSuitEconomyRooms.get(remainingPremiumRooms - 1);
      budgetsSuitEconomyRooms =
          getBudgetsSuitFreeRooms(
              occupancyRequest.getFreeEconomyRooms(), budget -> budget < updatedMinBudgetLimit);
    }
    return new RoomOccupancyResponse(
        getFormattedOccupancyUsage(budgetsSuitPremiumRooms),
        getFormattedOccupancyUsage(budgetsSuitEconomyRooms));
  }

  private String getFormattedOccupancyUsage(List<Integer> budgetsSuitPremiumRooms) {
    return String.format(
        USAGE_REPRESENTATION_TEMPLATE,
        budgetsSuitPremiumRooms.size(),
        budgetsSuitPremiumRooms.stream().reduce(0, Integer::sum));
  }

  private List<Integer> getBudgetsSuitFreeRooms(
      int freeRooms, Predicate<Integer> predicatedExpression) {
    List<Integer> budgetsSuitPremiumCapacity =
        guestRepository
            .getGuestsPayBudget()
            .stream()
            .filter(predicatedExpression)
            .sorted(Comparator.reverseOrder())
            .limit(freeRooms)
            .collect(Collectors.toList());
    return budgetsSuitPremiumCapacity;
  }
}
