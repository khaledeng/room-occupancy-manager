package com.smarthost.roomservice.occupancymanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;
import com.smarthost.roomservice.occupancymanagement.repository.GuestRepository;
import com.smarthost.roomservice.occupancymanagement.service.impl.RoomOccupancyMGMServiceImpl;

@ExtendWith(MockitoExtension.class)
public class RoomOccupancyMGMServiceTest {

  @InjectMocks private RoomOccupancyMGMServiceImpl roomOccupancyMGMService;

  @Mock GuestRepository guestRepository;

  @Test
  void testOccupyRoomsCase1() {
    when(guestRepository.getGuestsPayBudget()).thenReturn(stubbedGuestsBudget());
    RoomOccupancyResponse occupancyResponse =
        roomOccupancyMGMService.occupyRooms(new RoomOccupancyRequest(3, 3));
    assertEquals("3 (EUR 738)", occupancyResponse.getPremiumUsage());
    assertEquals("3 (EUR 167)", occupancyResponse.getEconomyUsage());
  }

  @Test
  void testOccupyRoomsCase2() {
    when(guestRepository.getGuestsPayBudget()).thenReturn(stubbedGuestsBudget());
    RoomOccupancyResponse occupancyResponse =
        roomOccupancyMGMService.occupyRooms(new RoomOccupancyRequest(7, 5));
    assertEquals("6 (EUR 1054)", occupancyResponse.getPremiumUsage());
    assertEquals("4 (EUR 189)", occupancyResponse.getEconomyUsage());
  }

  @Test
  void testOccupyRoomsCase3() {
    when(guestRepository.getGuestsPayBudget()).thenReturn(stubbedGuestsBudget());
    RoomOccupancyResponse occupancyResponse =
        roomOccupancyMGMService.occupyRooms(new RoomOccupancyRequest(2, 7));
    assertEquals("2 (EUR 583)", occupancyResponse.getPremiumUsage());
    assertEquals("4 (EUR 189)", occupancyResponse.getEconomyUsage());
  }

  @Test
  void testOccupyRoomsCase4() {
    when(guestRepository.getGuestsPayBudget()).thenReturn(stubbedGuestsBudget());
    RoomOccupancyResponse occupancyResponse =
        roomOccupancyMGMService.occupyRooms(new RoomOccupancyRequest(7, 1));
    assertEquals("7 (EUR 1153)", occupancyResponse.getPremiumUsage());
    assertEquals("1 (EUR 45)", occupancyResponse.getEconomyUsage());
  }

  private List<Integer> stubbedGuestsBudget() {
    return Arrays.asList(23, 45, 155, 374, 22, 99, 100, 101, 115, 209);
  }
}
