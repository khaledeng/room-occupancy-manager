package com.smarthost.roomservice.occupancymanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.smarthost.roomservice.occupancymanagement.controller.impl.RoomOccupancyMGMControllerImpl;
import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;
import com.smarthost.roomservice.occupancymanagement.service.RoomOccupancyMGMService;

@ExtendWith(MockitoExtension.class)
public class RoomOccupancyMGMControllerTest {

  @InjectMocks private RoomOccupancyMGMControllerImpl controller;

  @Mock RoomOccupancyMGMService mgmService;

  private static final String MOCKED_PREMUIM_USAGE = "3 (EUR 738)";
  private static final String MOCKED_ECONOMY_USAFE = "3 (EUR 167)";

  @Test
  void testOccupyRoomsCase1() {
    when(mgmService.occupyRooms(any(RoomOccupancyRequest.class)))
        .thenReturn(new RoomOccupancyResponse(MOCKED_PREMUIM_USAGE, MOCKED_ECONOMY_USAFE));
    RoomOccupancyResponse occupancyResponse =
        controller.roomOccupancy(new RoomOccupancyRequest(3, 3));
    assertEquals(MOCKED_PREMUIM_USAGE, occupancyResponse.getPremiumUsage());
    assertEquals(MOCKED_ECONOMY_USAFE, occupancyResponse.getEconomyUsage());
  }
}
