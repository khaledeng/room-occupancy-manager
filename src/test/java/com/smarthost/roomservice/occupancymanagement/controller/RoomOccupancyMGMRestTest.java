package com.smarthost.roomservice.occupancymanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.IOException;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.smarthost.roomservice.occupancymanagement.RoomOccupancyManagerApplication;
import com.smarthost.roomservice.occupancymanagement.model.request.RoomOccupancyRequest;
import com.smarthost.roomservice.occupancymanagement.model.response.RoomOccupancyResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = RoomOccupancyManagerApplication.class)
@AutoConfigureMockMvc
public class RoomOccupancyMGMRestTest {

  @Autowired private MockMvc mockMvc;
  @Autowired private ObjectMapper objectMapper;

  private static final String ROOM_OCCUPANCY_ENDPOINT = "/room-management/occupancy";

  @Test
  public void validateRoomOccupancyCase1() throws Exception {
    RoomOccupancyResponse occupancyResponse = performSucccessfulRoomOccupancy(3, 3);
    assertEquals("3 (EUR 738)", occupancyResponse.getPremiumUsage());
    assertEquals("3 (EUR 167)", occupancyResponse.getEconomyUsage());
  }

  @Test
  public void validateRoomOccupancyCase2() throws Exception {
    RoomOccupancyResponse occupancyResponse = performSucccessfulRoomOccupancy(7, 5);
    assertEquals("6 (EUR 1054)", occupancyResponse.getPremiumUsage());
    assertEquals("4 (EUR 189)", occupancyResponse.getEconomyUsage());
  }

  @Test
  public void validateRoomOccupancyCase3() throws Exception {
    RoomOccupancyResponse occupancyResponse = performSucccessfulRoomOccupancy(2, 7);
    assertEquals("2 (EUR 583)", occupancyResponse.getPremiumUsage());
    assertEquals("4 (EUR 189)", occupancyResponse.getEconomyUsage());
  }

  @Test
  public void validateRoomOccupancyCase4() throws Exception {
    RoomOccupancyResponse occupancyResponse = performSucccessfulRoomOccupancy(7, 1);
    assertEquals("7 (EUR 1153)", occupancyResponse.getPremiumUsage());
    assertEquals("1 (EUR 45)", occupancyResponse.getEconomyUsage());
  }

  private RoomOccupancyResponse performSucccessfulRoomOccupancy(
      int freePremiumRooms, int freeEconomyRooms) throws IOException, Exception {
    log.info(
        "Validating Case with ({}) free premium rooms and ({}) free economy rooms",
        freePremiumRooms,
        freeEconomyRooms);
    RoomOccupancyRequest occupancyRequest =
        RoomOccupancyRequest.builder()
            .freePremiumRooms(freePremiumRooms)
            .freeEconomyRooms(freeEconomyRooms)
            .build();
    MvcResult mvcResult =
        performHttpRequest(HttpMethod.POST, convertObjectToJson(occupancyRequest), status().isOk());

    return convertJsonToObject(
        mvcResult.getResponse().getContentAsString(), RoomOccupancyResponse.class);
  }

  @Test
  public void validateOccupancyEmptyRequest() throws Exception {
    performHttpRequest(HttpMethod.POST, "{}", status().isBadRequest());
  }

  @Test
  public void validateOccupancyNoBody() throws Exception {
    performHttpRequest(HttpMethod.POST, "", status().isBadRequest());
  }

  @Test
  public void validateOccupancyInvalidInput() throws Exception {
    performHttpRequest(HttpMethod.POST, "{NotValidInput:", status().isBadRequest());
  }

  @Test
  public void validateOccupancyBadDataType() throws Exception {
    performHttpRequest(
        HttpMethod.POST,
        "{\"free-premium-rooms\": \"Wrong-Input-DataType\" , \"free-economy-rooms\": \"Wrong-DataType\"}",
        status().isBadRequest());
  }

  @Test
  public void validateOccupancyEmptyEconomy() throws Exception {
    performHttpRequest(
        HttpMethod.POST,
        convertObjectToJson(RoomOccupancyRequest.builder().freePremiumRooms(3).build()),
        status().isBadRequest());
  }

  @Test
  public void validateOccupancyEmptyPremium() throws Exception {
    performHttpRequest(
        HttpMethod.POST,
        convertObjectToJson(RoomOccupancyRequest.builder().freeEconomyRooms(3).build()),
        status().isBadRequest());
  }

  @Test
  public void validateOccupancyNegativeRoom() throws Exception {
    performHttpRequest(
        HttpMethod.POST,
        convertObjectToJson(RoomOccupancyRequest.builder().freeEconomyRooms(-1).build()),
        status().isBadRequest());
  }

  @Test
  public void validateOccupancyMissedContentType() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.request(HttpMethod.POST, ROOM_OCCUPANCY_ENDPOINT)
                .content(convertObjectToJson(new RoomOccupancyRequest(3, 3)))
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(status().isUnsupportedMediaType())
        .andReturn();
  }

  @Test
  public void validateOccupancyWrongContentType() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.request(HttpMethod.POST, ROOM_OCCUPANCY_ENDPOINT)
                .contentType(MediaType.APPLICATION_XML)
                .content(convertObjectToJson(new RoomOccupancyRequest(3, 3)))
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(status().isUnsupportedMediaType())
        .andReturn();
  }

  private MvcResult performHttpRequest(
      HttpMethod httpMethod, String requestBody, ResultMatcher expectedResponseType)
      throws Exception {
    return mockMvc
        .perform(
            MockMvcRequestBuilders.request(httpMethod, ROOM_OCCUPANCY_ENDPOINT)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(requestBody)
                .accept(MediaType.APPLICATION_JSON_VALUE))
        .andDo(print())
        .andExpect(expectedResponseType)
        .andReturn();
  }

  public <T> T convertJsonToObject(String jsonString, Class<T> responseType) throws IOException {
    return objectMapper.readValue(jsonString, responseType);
  }

  public String convertObjectToJson(Object object) throws IOException {
    return objectMapper.writeValueAsString(object);
  }
}
